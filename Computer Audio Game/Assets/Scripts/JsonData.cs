﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonData
{
    string path;
    private GameData gameData;

    // Default contructor will create a gameData instance that contains info from data.js
    public JsonData()
    {
        path = Application.dataPath + "/Scripts/data.js";
        string content = System.IO.File.ReadAllText(path);
        gameData = JsonUtility.FromJson<GameData>(content);
    }

    public GameData getGameData()
    {
        return gameData;
    }
}
