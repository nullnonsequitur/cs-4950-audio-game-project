using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum Direction {UP = 0, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, UP_LEFT}; //clockwise

public class PlayerController : MonoBehaviour
{
    private Direction playerCurrentLane; // lane the player should currently be facing
    private Vector2 movement;
    private float playerRotation;
    public int score;
    public Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        playerCurrentLane = Direction.UP;
        playerRotation = transform.position.y;
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // Deal with player input
        float inputX = Input.GetAxisRaw("Horizontal");
        float inputY = Input.GetAxisRaw("Vertical");
        movement = new Vector2(inputX, inputY);
        //Diagonals
        if (inputX != 0 && inputY != 0) {
            if (movement.y == 1 && movement.x == -1) {
                playerCurrentLane = Direction.UP_LEFT;
                transform.localEulerAngles = new Vector3(0,-45,0);
                // print("UP_LEFT");
            }

            if (movement.y == 1 && movement.x == 1) {
                playerCurrentLane = Direction.UP_RIGHT;
                transform.localEulerAngles = new Vector3(0,45,0);
                // print("UP_RIGHT");
            }

            if (movement.y == -1 && movement.x == -1) {
                playerCurrentLane = Direction.DOWN_LEFT;
                transform.localEulerAngles = new Vector3(0,-135,0);
                // print("DOWN_LEFT");
            }

            if (movement.y == -1 && movement.x == 1) {
                playerCurrentLane = Direction.DOWN_RIGHT;
                transform.localEulerAngles = new Vector3(0,135,0);
                // print("DOWN_RIGHT");
            }
        } else {
            //left/right/up/down
            if (movement.x == -1) {
                playerCurrentLane = Direction.LEFT;
                transform.localEulerAngles = new Vector3(0,-90,0);
                // print("LEFT");
            }

            if (movement.x == 1) {
                playerCurrentLane = Direction.RIGHT;
                transform.localEulerAngles = new Vector3(0,90,0);
                // print("RIGHT");
            }


            if (movement.y == 1) {
                playerCurrentLane = Direction.UP;
                transform.localEulerAngles = new Vector3(0,0,0);
                // print("UP");
            }


            if (movement.y == -1) {
                playerCurrentLane = Direction.DOWN;
                transform.localEulerAngles = new Vector3(0,180,0);
                // print("DOWN");
            }
        }

        //Deal with score display
        scoreText.text = "Score: " + score;
    }

    public Direction getPlayerCurrentLane() {
        return playerCurrentLane;
    }
}
