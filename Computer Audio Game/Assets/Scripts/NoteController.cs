﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// container class for note data cuz unity wont let me use arrays with different types
public class NoteData {
    public Direction lane;
    public float timestamp;

    public NoteData(Direction lane, float timestamp) {
        this.lane = lane;
        this.timestamp = timestamp;
    }
}


// Meant to be used by NoteSpawn GameObject
public class NoteController : MonoBehaviour
{
    // Note spawn points
    public GameObject noteSpawnN;
    public GameObject noteSpawnNE;
    public GameObject noteSpawnE;
    public GameObject noteSpawnSE;
    public GameObject noteSpawnS;
    public GameObject noteSpawnSW;
    public GameObject noteSpawnW;
    public GameObject noteSpawnNW;
    public GameObject notePrefab;

    // reference to music source
    public AudioSource music;
    // reference to bomb sfx
    public AudioSource boom;

    // reference to player controller
    public PlayerController pController;
    // reference to note animation, used to get animation length
    public AnimationClip noteAnim;

    // Title text
    public Text titleText;
    public Text enterText;
    // instance of jsonData and gameData object to read in lanes and time info from data.js
    public JsonData jsonData;
    public GameData gameData;
    public System.Random rnd = new System.Random();

    // has the song started
    private bool started;
    private float timer = 0f;
    // array of all notes consisting of assigned lanes and timestamps
    private List<NoteData> noteList;
    // Bomb stuff
    private float bombChance = 0.45f; //percent chance a note will become a bomb.
    private int bombsToSpawn = 6;
    private float lastBombTimestamp = 0.0f;
    private float bombMinSpace = .5f;

    void Start() {
        started = false;
        noteList = new List<NoteData>();

        queueNotes(); //queue up notes to be used when song starts
    }

    void Update() {
        if (!started) {
            if (Input.GetKeyUp(KeyCode.Return)) {
                started = true;
                //Queue up all the notes in the note list to be spawned
                foreach (NoteData note in noteList) {
                    float timeInSecs = note.timestamp; //the time float
                    Invoke("spawnNextNote", timeInSecs);
                }
                // Start the music with animation delay
                Invoke("startMusic", noteAnim.length);
                //hide title text
                titleText.gameObject.SetActive(false);
                enterText.gameObject.SetActive(false);
                // reset timer
                timer = 0f;
            }
        } else {
            //Check if song is completed. If it is, return to title.
            timer += Time.deltaTime;
            if (timer > music.clip.length + 2) {
                started = false;
                // re-enable title screen gui
                titleText.gameObject.SetActive(true);
                enterText.gameObject.SetActive(true);
                // reset score
                pController.score = 0;
                // requeue up notes
                queueNotes();
                //reset bomb timer
                lastBombTimestamp = 0.0f;
            }
        }
    }

    private void queueNotes() {
        //Creating notes according to the data read in from data.js
        jsonData = new JsonData();
        gameData = jsonData.getGameData();
        for (int i = 0; i < gameData.timestampArray.Length; i++)
        {
            noteList.Add(new NoteData((Direction) (rnd.Next(8)), gameData.timestampArray[i]));
        }
    }

    //Spawns whatever note is next in the note list
    private void spawnNextNote() {
        // grab first note off of ordered list and spawn
        var noteToSpawn = noteList[0];
        Direction lane = noteToSpawn.lane; //the direction

        // determine if note is a bomb
        float bombRoll; // roll dice on whether this is bomb
        if (timer - bombMinSpace > lastBombTimestamp) {
            bombRoll = Random.value; // bombs are ok at the moment
        } else {
            bombRoll = 1.0f; //too many bombs in quick succession, cant be a bomb
        }

        if (bombRoll < bombChance) {
            // note is bomb, spawn series of bombs
            spawnNote(lane, true);
            // spawn additional bombs on subsequent lanes. It does this by converting the Direction to an
            // int, adding an offset, modding by 8 to deal with wraparound, then converts back to Direction
            for (int i = 1; i < bombsToSpawn; i++) {
                spawnNote((Direction) (((int)lane + i) % 8), true);
            }
            lastBombTimestamp = timer;
        } else {
            // note is not bomb
            spawnNote(lane, false);
        }

        // "pop" the note just spawned
        noteList.RemoveAt(0);
    }

    // instantiate new note object at proper spawn position
    private void spawnNote(Direction dir, bool isBomb) {
        GameObject newNote = null;
        // create note at proper lane position
        switch (dir) {
            case Direction.UP:
                newNote = Instantiate(notePrefab, noteSpawnN.transform);
                break;
            case Direction.UP_RIGHT:
                newNote = Instantiate(notePrefab, noteSpawnNE.transform);
                break;
            case Direction.RIGHT:
                newNote = Instantiate(notePrefab, noteSpawnE.transform);
                break;
            case Direction.DOWN_RIGHT:
                newNote = Instantiate(notePrefab, noteSpawnSE.transform);
                break;
            case Direction.DOWN:
                newNote = Instantiate(notePrefab, noteSpawnS.transform);
                break;
            case Direction.DOWN_LEFT:
                newNote = Instantiate(notePrefab, noteSpawnSW.transform);
                break;
            case Direction.LEFT:
                newNote = Instantiate(notePrefab, noteSpawnW.transform);
                break;
            case Direction.UP_LEFT:
                newNote = Instantiate(notePrefab, noteSpawnNW.transform);
                break;
        }
        // populate some fields on new spawned note (couldnt find a better way to do it)
        Note newNoteScript = newNote.GetComponent<Note>();
        newNoteScript.whichLane = dir;
        newNoteScript.controller = this;
        newNoteScript.isBomb = isBomb;
        if (isBomb) {
            newNote.GetComponent<Renderer>().material.color = new Color(1, 0, 0, 1); //change color for bombs
        }

    }

    // Simple method to start music, needed to use with Invoke
    private void startMusic() {
        music.Play();
    }

    public void checkNoteHit(Direction noteLane, bool wasBomb) {
        //Check if player is currently blocking the lane the note has triggered
        Direction playerLane = pController.getPlayerCurrentLane();
        if (noteLane == playerLane) {
            if (!wasBomb) {
                print("Hit note on: " + noteLane);
                pController.score += 100;
            } else {
                print("Hit bomb on: " + noteLane);
                pController.score -= 100;
                boom.Play(); //play explosion sfx
            }
        } else {
            // if (!wasBomb) {
            //     print("Missed note on: " + noteLane);
            // } else {
            //     print("Dodged bomb on: " + noteLane);
            // }
        }
    }
}

