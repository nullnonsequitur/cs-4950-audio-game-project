using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    // which lane this note runs on
    public Direction whichLane;
    // reference to the note controller
    public NoteController controller;
    // is this a bomb?
    public bool isBomb;

    public void animFinished()    {
        // tell NoteController the animation is finished
        // pass in which lane this note came from
        controller.checkNoteHit(whichLane, isBomb);

        // destroy self
        Destroy(gameObject);
    }
}
