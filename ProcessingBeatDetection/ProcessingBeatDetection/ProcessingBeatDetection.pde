import beads.*;
import controlP5.*;
import java.util.Arrays;

ControlP5 p5;
PowerSpectrum ps;
Gain masterGain;
ShortFrameSegmenter sfs;
FFT fft;
SamplePlayer music;
Bead endListener;
BiquadFilter filter;
SpectralDifference sd;
PeakDetector pd;
float beatTime;

boolean printed = false;
PrintWriter output;

color fore = color(255,200,160);
color back = color(0,0,0);
void setup()
{
   size(600,600);
   
   //Setting up comtrolp5 components
   p5 = new ControlP5(this);
   
   p5.addButton("NoFilter")
    .setPosition(width/6 - 150/2, height/12 - 60/2)
    .setSize(150,60)
    .activateBy((ControlP5.RELEASE))
    .setColorActive(color(255,200,160))
    .setLabel("No Filter");
    
   p5.addButton("LowPassFilter")
    .setPosition(width/2 - 150/2, height/12 - 60/2)
    .setSize(150,60)
    .activateBy((ControlP5.RELEASE))
    .setColorActive(color(255,200,160))
    .setLabel("Low Pass Filter");
    
   p5.addButton("HighPassFilter")
    .setPosition(width/6 - 150/2, 3*height/12 - 60/2)
    .setSize(150,60)
    .activateBy((ControlP5.RELEASE))
    .setColorActive(color(255,200,160))
    .setLabel("High Pass Filter");
    
   p5.addButton("BandPassFilter")
    .setPosition(width/2 - 150/2, 3*height/12 - 60/2)
    .setSize(150,60)
    .activateBy((ControlP5.RELEASE))
    .setColorActive(color(255,200,160))
    .setLabel("Band Pass Filter");
    
   p5.addSlider("Frequency")
    .setPosition(5*width/6 - 85, height/12 - 60/2)
    .setSize(40,260)
    .setRange(1,21500)
    .setValue(7000)
    .setColorActive(color(255,200,160));
   
   p5.addSlider("Bandwidth")
    .setPosition(5*width/6 + 5, height/12 - 60/2)
    .setSize(40,260)
    .setRange(0,100)
    .setValue(20)
    .setColorActive(color(255,200,160));
  
  
   //Initiallizing all components
   ac = new AudioContext();
   masterGain = new Gain(ac, 2, 0.5);
   fft = new FFT();
   ps = new PowerSpectrum();
   ShortFrameSegmenter sfs = new ShortFrameSegmenter(ac);
   //Audio input
   try { 
    music = getSamplePlayer("redial.wav",false);
  } catch(Exception e) {
    e.printStackTrace();
  }
  filter = new BiquadFilter(ac,1);
  filter.setType(BiquadFilter.LP);
  filter.setFrequency(217f);
  
  
  endListener = new Bead() {
    public void messageReceived(Bead message) {
        SamplePlayer sp = (SamplePlayer) message;
        
        // reset sp to play again from start
        if (!printed) {
          output.println("]");
          output.println("}");
          output.close();
          printed = true;
        }
    }
  };
  sd = new SpectralDifference(ac.getSampleRate());
  pd = new PeakDetector();
  pd.setThreshold(0.8f);
  pd.setAlpha(0.1f);
  
  pd.addMessageListener
  (
new Bead() {
      protected void messageReceived(Bead b)
      {
        if (music.getPosition()/1000f - beatTime > 0.25) {
          println(music.getPosition()/1000f);
          output.print("," + music.getPosition() / 1000f);
          beatTime = (float) music.getPosition()/1000f;
      }
      }
} );

  output = createWriter("data.js");
  output.println("{");
  output.print("  \"timestampArray\" : [0");
  
  beatTime = 0f;
  
  //Connecting the graph components that doens't change
  ac.out.addInput(masterGain);
  sfs.addInput(ac.out);
  sfs.addListener(fft);
  fft.addListener(ps);
  ps.addListener(sd);
  sd.addListener(pd);
  ac.out.addDependent(sfs);
  music.setEndListener(endListener);
  filter.addInput(music);
  masterGain.addInput(filter);
  ac.start();
}

void draw() {
  
  background(back);
  stroke(fore);
  float[] features = ps.getFeatures();
  
  if (features != null) {
    for (int x = 0; x < (width); x++) {
    int featureIndex = (x * features.length)/(width);
    int barHeight = Math.min((int)(features[featureIndex] * 300), 300 - 1);
    line(x, 600, x, 600 - barHeight);
    }
  }
}

void NoFilter() {
  masterGain.clearInputConnections();
  filter.setType(BiquadFilter.AP);
  masterGain.addInput(filter);
}

void LowPassFilter() {
  masterGain.clearInputConnections();
  filter.setType(BiquadFilter.LP);
  masterGain.addInput(filter);
}

void HighPassFilter() {
  masterGain.clearInputConnections();
  filter.setType(BiquadFilter.HP);
  masterGain.addInput(filter);
}

void BandPassFilter() {
  masterGain.clearInputConnections();
  filter.setType(BiquadFilter.BP_SKIRT);
  masterGain.addInput(filter);
}

void Frequency(float value) {
  filter.setFrequency(value);
}

void Bandwidth(float value) {
  filter.setQ(value/100);
}
